Welcome to django-rest-auth
===========================


Django-rest-auth provides a set of REST API endpoints for Authentication and Registration


Documentation
-------------
https://www.django-rest-framework.org/tutorial/quickstart/


Source code
-----------
https://gitlab.com/ak080495/foodiesplace-django.git


