



from django.db import models


from django.contrib.auth.models import AbstractUser



class File(models.Model):
    file = models.FileField(blank=False, null=False)
    def __str__(self):
        return self.file.name







class User(AbstractUser):
    email = models.EmailField(unique=True)
    file = models.FileField(blank=True, null=True)
    # pic = models.ImageField(upload_to='pics', blank=True)
    adress = models.CharField(max_length=100, blank=True)
    pin = models.CharField(max_length=6, blank=True)
    country = models.CharField(max_length=20, blank=True)
    city = models.CharField(max_length=20, blank=True)
    about = models.CharField(max_length=100, blank=True)
    pass






class Meal(models.Model):
    name = models.CharField(max_length=200)
    price = models.TextField(blank=True)
    
    pic = models.ImageField(upload_to='pics', blank=True)
    field = models.CharField(max_length=50,default='meal', editable=False)



class Food(models.Model):
    name = models.CharField(max_length=200)
    price = models.TextField(blank=True)

    pic = models.ImageField(upload_to='pics', blank=True)
    field = models.CharField(max_length=50,default='food', editable=False)
  



class BEVERAGES(models.Model):
    name = models.CharField(max_length=200)
    price = models.TextField(blank=True)

    pic = models.ImageField(upload_to='pics', blank=True)
    field = models.CharField(max_length=50,default='bevrage', editable=False)
 


class COMBOS(models.Model):
    name = models.CharField(max_length=200)
    price = models.TextField(blank=True)

    pic = models.ImageField(upload_to='pics', blank=True)
    field = models.CharField(max_length=50,default='combo', editable=False)
   


class Slider(models.Model):
    title = models.CharField(max_length=200)
    alt = models.CharField(max_length=200)

    image = models.ImageField(upload_to='pics', blank=True)

    thumbImage = models.ImageField(upload_to='pics', blank=True)



class Gallery(models.Model):
  
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='pics', blank=True)

  


class GalleryVideo(models.Model):
  
    Videoaddress = models.CharField(max_length=200)








class Consult(models.Model):

    Feedback = 'Feedback'
    Report = 'Report a bug'
    Feature = 'Feature request'
  
    SUBJECT_CHOICES = [
        (Feedback, 'Feedback'),
        (Report, 'Report a bug'),
        (Feature, 'Feature request'),
        
    ]




    name = models.CharField(max_length=30,blank=True)
    subject = models.CharField(max_length=30,choices=SUBJECT_CHOICES,blank=True)
    message = models.TextField(max_length=500,blank=True)
    from_email = models.CharField(max_length=30,blank=True)










