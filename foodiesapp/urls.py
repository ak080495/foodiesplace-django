from django.urls import path,include

from django.contrib.auth import views as auth_views

from django.conf import settings
from rest_framework import routers
from . import *

router = routers.SimpleRouter()

router.register(r'meal', views.MealViewSet)
router.register(r'food', views.FoodViewSet)
router.register(r'bevrage', views.BEVERAGESViewSet)
router.register(r'combo', views.COMBOSViewSet)
router.register(r'slider', views.SliderViewSet)
router.register(r'gallery', views.GalleryViewSet)
router.register(r'galleryvideo', views.GalleryVideoViewSet)
router.register(r'contact', views.ConsultViewSet)
router.register(r'api/users', views.UserViewSet)
# router.register(r'file', views.FileViewSet)


urlpatterns =[
    path('',include(router.urls)),
    path('api/pwd/', include('rest_auth.urls')),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), {'template_name':'registration/Reset_Email_Sent.html'}, name='password_reset_done'),
    path('<uidb64>', auth_views.PasswordResetConfirmView.as_view(), {'template_name' : 'registration/Forgot_password.html'}, name='password_reset_confirm'),
     path('<token>/', auth_views.PasswordResetConfirmView.as_view(), {'template_name' : 'registration/Forgot_password.html'}, name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), {'template_name' : 'registration/Signin.html'}, name='password_reset_complete'),
]